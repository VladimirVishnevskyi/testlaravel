<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/product/new','ProductController@create');
Route::delete('/product/{id}','ProductController@destroy');

Route::post('/product/{id}/to-order','OrderController@toOrder');


Route::get('/product/{id}/review','ReviewProductController@list');
Route::post('/product/{id}/new-review','ReviewProductController@new');
Route::post('/review/{id}/like','ReviewProductController@Like');
Route::post('/review/{id}/delete-like','ReviewProductlikeController@deleteLike');


Route::post('/product/{id}/add-wishlist','WishlistController@add');


Route::put('/product/{id}/cover-image','ProductCoverImageController@update');



Route::post('/pay/{id}/applePay','PayMethodController@ApplePay');
Route::post('/pay/{id}/stripe','PayMethodController@Stripe');
Route::post('/pay/{id}/PayPal','PayMethodController@PayPal');


















Route::get('/','MainController@all');
Route::get('/category','CategoryController@all');
Route::get('/classic', 'ProductController@all');

